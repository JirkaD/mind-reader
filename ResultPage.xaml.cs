﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace MindReader
{
    public partial class ResultPage : PhoneApplicationPage
    {
        // Constructor
        public ResultPage()
        {
            InitializeComponent();
            ReadingMind.Begin();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void Rate_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
                marketplaceReviewTask.Show();
            }
            catch
            {

            }
        }
    }
}